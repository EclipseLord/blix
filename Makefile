
all: clean build image 

build:
	$(MAKE) -C src/kernel

clean:
	$(MAKE) -C src/kernel clean
	rm -f blix.iso

image:
	grub-mkrescue -o ./blix.iso ./bin/

run:
	qemu-system-x86_64 blix.iso -m 512 -serial stdio	

debug:
	qemu-system-x86_64 blix.iso -m 512 -serial stdio -s -S & disown
	gdb -iex 'add-auto-load-safe-path .'
