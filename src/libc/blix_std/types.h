#ifndef LIBC_TYPES_H
#define LIBC_TYPES_H

typedef signed char int8_t;
typedef short int16_t;
typedef long int32_t;
typedef long long int64_t;

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned long uint32_t;
typedef unsigned long long uint64_t;

// pointer types
typedef long intptr_t;
typedef unsigned long uintptr_t;

#endif
