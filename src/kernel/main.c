// kernel
#include "io.h"
#include "print.h"

// drivers
#include <graphics/vga/vga.h>

typedef struct GDTEntry { 
	unsigned int lo, hi;
} gdtentry_t;

typedef struct GDT {
	gdtentry_t null;
	gdtentry_t boot_cs;
	gdtentry_t boot_ds;
} gdt_t;


void blix_main() {					
	init_serial_port(QEMU_SERIAL);
	
	printk("Blix is now in x64 long mode, woot woot!\n");	
	// interrupts
}
