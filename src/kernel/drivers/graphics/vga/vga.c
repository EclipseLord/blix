#include "vga.h"

static volatile char* video_memory = (volatile char*)0xb8000;

void vga_putchar(int color, char ch) {
	*video_memory++ = ch;
	*video_memory++ = color;
}
